# Sequence Analyses for Siebert at al. manuscript

This repository contains the data and scripts that were used to build the phylogenetic trees for:

Siebert, S, FE Goetz, SH Church, P Bhattacharyya, F Zapata, SHD Haddock, and CW Dunn. (preprint) Stem Cells in a Colonial Animal with Localized Growth Zones. bioRxiv preprint: [doi:10.1101/001685](http://dx.doi.org/10.1101/001685)</a>.

## Contents

The included files are:

- `original_sequences.fasta`, the newly sequenced genes for *Nanomia bijuga*. These will be submitted to NCBI.

- `nanos.fasta`, `piwi.fasta`, and `vasa_pl10.fas`, fasta files of protein sequences included in the phylogenetic analyses.

- `analyses.sh`, the shell script used to execute the analyses. It includes the SLURM job manager settings specific to our cluster, which can be deleted or modified for use on other clusters.

- `nanos.tre`, `piwi.tre`, and `vasa_pl10.tre`, the trees that were produced by the analyses, with bootstrap support values. These trees are graphically presented in the manuscript as Supplementary Figure S2.