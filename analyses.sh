#!/bin/bash
#SBATCH -J StemCells
#SBATCH -c 16
#SBATCH -t 96:00:00
#SBATCH --mem=62G
#SBATCH -e stemcells-%j.out
#SBATCH -o stemcells-%j.out

set -e

## This script aligns and generates Maximum Likelihood gene trees for four
## gene families expressed in stem cells in Nanomia: Nanos, Piwi, PL10, and Vasa.
## To align the sequences, it uses MUSCLE http://www.drive5.com/muscle/
## doi:10.1186/1471-2105-5-113
## For phylogenetic analysis, it uses RAxML http://www.exelixis-lab.org/
## doi:10.1093/bioinformatics/btl446
## For leaf stability analysis, it uses phyutility http://code.google.com/p/phyutility/
## doi:10.1093/bioinformatics/btm619

## Sequences in each fasta file were recovered from significant 
## blastp hits against refseq and complemented with sequences from
## doi:10.1093/molbev/msr046

module load muscle/3.8.31 raxml phyutility/2.2.6

# Alignment
for FILE in *.fas
  do
    BASENAME=${FILE%.*}
    muscle -in $FILE -fastaout $BASENAME.afas &> /dev/null
 done 

# Change file format
for FILE in *.afas
  do
    BASENAME=${FILE%.*}
    awk '{if (NR==1 && $0 ~/>/){print$0;next}if($0~/^>/){print"\n"$0;next}else{printf("%s",$0)}}' $FILE > $BASENAME.mafas
  done

# This loop is modified from:
# http://taxevo.wordpress.com/2011/09/23/converting-fasta-to-phylip-using-bash/
for FILE in *.mafas
  do
    BASENAME=${FILE%.*}
	NUMSEQS=$(grep '>' -c $FILE)
	NUMBASES=$(grep -m 1 -v '>' $FILE | expr `wc -m` - 1)
	echo $NUMSEQS $NUMBASES | awk '{ print "\t"$1"\t"$2 }' > $BASENAME.phy
	cat $FILE | sed 's/>//g' |  xargs -n 2 | awk '{ print $1" "$2" " $3 }' >> $BASENAME.phy
  done

# Clean up
rm *.afas *.mafas

# Phylogenetic analysis
for FILE in *.phy
  do
    BASENAME=${FILE%.*}
    raxmlHPC -s $FILE -n $BASENAME -m PROTGAMMAWAG -p $RANDOM -x $RANDOM -N 500 -f a &> /dev/null
  done

# Leaf Stabiliy
for FILE in RAxML_bootstrap.*
  do
    BASENAME=`echo $FILE | awk -F . '{print $NF}'`
    phyutility -ls -in $FILE 1> $BASENAME.leafstab
  done


